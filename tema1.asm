%include "io.inc"

%define MAX_INPUT_SIZE 4096
section .data
        cuv dd 0
        nr dd 0
        plus dd 0
        steluta dd 0
        minus dd 0
        slash dd 0
        intamplare dd 0
        mama dd 0
        expresiecurenta dd 0
section .bss
	expr: resb MAX_INPUT_SIZE

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
   ; mov ebp, esp; for correct debugging
;    mov ebp, esp; for correct debugging
        push ebp
        mov ebp, esp

	GET_STRING expr, MAX_INPUT_SIZE
        mov byte[nr],0 ;initializez cu 0 variabila nr, care se va comporta ca un contor
        mov ebx,0
        mov byte[intamplare],0 ;variabila care verifica ca nu cumva sa continui parcurgerea daca inputul s-a terminat
   ;aflu nr de caractere al inputului, pe care il folosesc ulterior ca conditie de stopare in nrcuvinte
strlen:

        mov edi, expr
        mov al, 0
        repne scasb
        sub edi, expr
        dec edi
        mov edx,0
        mov ecx,edi
        xor ebx,ebx
        mov ebx, 0
        mov al,' '
        mov edi,expr
        
nrcuvinte: 

;aflu cate cuvinte am in string, folosind numarul de aparitii al caracterului "spatiu"
        repne scasb
        add ebx,1
        cmp ecx,0 ; in ecx am strlen
        jg nrcuvinte
        mov [cuv],ebx      
        mov edi,expr ;mut string-ul in edi, registrul cu care voi lucra
        mov [expresiecurenta],edi
        
cuvanturmator:

        inc byte[nr] ;cresc numarul cuvintelor, am trecut la cuv urmator
;fac un fel de string tokenizer, separ stringul expr in cuvinte, in functie de caracterul spatiu
      
        mov esi,edi ; in esi voi tine cuvantul curent
;variabilele plus,minus,steluta si slash sunt semafoare pentru a verifica daca in expresia mea nu am un numar, al carei valori zecimale e egala cu una dintre valorile zecimale ale lui +,-,/ sau *
        mov byte[plus],0
        mov byte[minus],0
        mov byte[steluta],0
        mov byte[slash],0
;ma asigur ca nu mai tin nimic in registrele ecx,eax 
        xor ecx,ecx
        xor eax,eax     
        mov ecx,0
        not ecx ;nestiind cate caractere are ecx, il initializez cu 0 si il neg, astfel ecx scazand fix cate caractere are cuvantul dintre spatii
        mov al, ' '
        cld
        repne scasb
; in ecx tin numarul de caractere al unui cuvant
       not ecx; pt a obtine efectiv nr de caract ale cuvantului
;tin in eax nr de caractere ale cuvantului  
       cmp ecx,10
       jge leax
       jl cc
 leax:
        
        inc byte[intamplare]
        mov eax,[intamplare]
        cmp byte[intamplare],1
        jg sfarsit
        jle cc
        
cc:

        xor eax,eax
        lea eax,[ecx-1]
;imi formez cuvantul in esi, terminandu-l cu "null"- pun null pe byte-ul de la adresa de inceput+nr de caractere ale cuvantului curent
;tata:
        mov byte [esi+eax], 0x00
        mov eax,[nr]
stringtoint: 
;fac convertirea din string in int a cuvantului curent  
        xor eax,eax
        
inceput:

;in ecx tin caracterul curent al cuvantului curent
        movzx ecx, byte[esi]
        movzx ebx, byte [esi+1]
        cmp byte[esi+1], 0 ; verific daca cuvantul curent are un singur caracter
        je continuare
        cmp ecx,'-' ;verific daca cuvantul incepe cu minus, astfel existand posibilitatea ca acesta sa fie un numar negativ sau caracterul '-'
        je posibilnegativ
        jne continuare
        
posibilnegativ: 
  
;verific daca urmatorul caracter este cifra, iar daca este, e clar ca nr e negativ        
        cmp ebx,'0'
        jge negative 
        jl continuare

negative:

;fac convertirea pentru nr negativ curent
        inc esi ;trec la urmatorul caracter din cuvant
        movzx ebx, byte[esi]
;verific daca caracterul este cifra, iar daca nu, inseamna ca am terminat de convertit nr,trec la negarea lui
        cmp ebx,'0'
        jb nega
        cmp ebx,'9'
        ja nega
        sub ebx,'0' ;transform in cifra caracterul curent
        imul eax,10 ; formez numarul in eax inmultindu-l cu 10 si apoi adaugand cifra din ebx
        add eax, ebx
        jmp negative  
              
nega:
     
        neg eax
        jmp convertire 
            
continuare:  
     
        inc esi
;verific daca caracterul curent este un operand
        cmp ecx, '+'
        je operatorplus
        cmp ecx,'-'
        je operatorminus
        cmp ecx,'*' 
        je operatorsteluta
        cmp ecx,'/'
        je operatorslash
        cmp ecx,'0'
        jb convertire
        cmp ecx,'9'
        ja convertire
        sub ecx,'0'
        imul eax,10
        add eax, ecx
        jmp inceput
        
;pentru fiecare operator, "activez" semaforul care imi spune ca cuvantul meu curent este operator        
operatorplus:  

;pun in eax valoarea hexa corespunzatoare fiecarei operatii  
        mov byte[plus],1
        mov eax,2BH ;
        jmp convertire
        
operatorminus:

        mov byte[minus],1
        mov eax,2DH
        jmp convertire
        
operatorsteluta:

        mov byte[steluta],1
        mov eax,2AH
        jmp convertire
        
operatorslash:

        mov byte[slash],1
        mov eax,2FH
        jmp convertire
        
convertire:

;verific mai intai daca cuvantul curent este o operatie
        cmp eax,2BH 
        je verificareadunare
        jne continuare1
        
verificareadunare:

        cmp byte[plus],1
        je adunare
        
continuare1:

        cmp eax, 2DH
        je verificarescadere
        jne continuare2
        
verificarescadere:

        cmp byte[minus],1
        je scadere
        
continuare2: 

        cmp eax, 2AH
        je verificareinmultire
        jne continuare3
        
verificareinmultire:

        cmp byte[steluta],1
        je inmultire
        
continuare3:
        
        cmp eax,2FH
        je verificareimpartire
        jne continuare4
verificareimpartire:

        cmp byte[slash],1
        je impartire
        
continuare4: 
       
;daca nu este operatie, pun numarul pe stiva
        push eax  
        mov edx,[nr] 
;verific daca am ajuns cumva la final, daca nu, trec la cuvantul urmator
        cmp edx, [cuv]
        jle cuvanturmator
        jg final

adunare:

        xor ebx,ebx        
        mov ebx,[nr]
        cmp ebx,[cuv] ;verific daca nu am ajuns la finalul expresie, pentru printare
        je printareadunare
        xor ebx,ebx
        pop ebx
        pop eax 
;scot ultimele 2 valori de pe stiva si fac adunarea, apoi pun nr rezultat in urma adunarii pe stiva
        add eax,ebx
        push eax
        jmp cuvanturmator
        
scadere:

        xor ebx,ebx
        mov ebx,[nr]
 ;acelasi lucru ca la adunare, dar scad cele 2 valori scoase de pe stiva    
        cmp ebx,[cuv]
        je printarescadere
        xor ebx,ebx
        pop ebx
        pop eax
        sub eax,ebx
        push eax
        jmp cuvanturmator
        
impartire:

        xor ebx,ebx
        mov ebx,[nr]
        cmp ebx,[cuv]
        je printareimpartire
        xor ebx,ebx
        pop ebx
        pop eax
        xor edx,edx ;ma asigur ca nu am nimic in edx, pentru impartire
        cdq
        idiv ebx
        push eax ;pun pe stiva valoarea obtinuta in urma impartirii
        jmp cuvanturmator
        
inmultire:

        xor ebx,ebx
        mov ebx,[nr]
        cmp ebx,[cuv]
        je printareinmultire
        xor ebx,ebx
        pop eax
        pop ebx
        inc edx
        imul ebx ;efectuez inmultirea celor 2 valori
        push eax
        jmp cuvanturmator
        
printareadunare: 

        pop eax
        pop ebx
        add eax,ebx
        test eax,eax ;verific daca rezultatul ultimei adunari este negativ sau pozitiv
        js negativadunare
        jns pozitivadunare
        
negativadunare: ; daca e negativ,il neg si adaug un minus in fata pt obtinerea rezultatului in forma corecta
        
        neg eax
        PRINT_STRING '-'
        PRINT_UDEC 4,eax
        jmp final
        
pozitivadunare:

        PRINT_UDEC 4,eax
        jmp final
        
printarescadere:

        pop eax
        pop ebx
        sub ebx, eax
        test ebx,ebx
;la fel procedez si pentru scadere     
        js negativscadere
        jns pozitivscadere
        
negativscadere:

        neg ebx
        PRINT_STRING '-'
        PRINT_UDEC 4,ebx
        jmp final
pozitivscadere: 

        PRINT_UDEC 4,ebx
        jmp final

printareimpartire:

        pop ebx
        pop eax
        xor edx,edx
        cdq
        idiv ebx
        PRINT_DEC 4,eax
        jmp final

printareinmultire:

        pop eax
        pop ebx
        imul ebx       
        PRINT_DEC 4,eax
        jmp final
        
sfarsit:

;cazul de exceptie in care scot valoarea de pe stiva, ea reprezentand rasp final
        pop eax
        test eax,eax
        js negtv
        jns pos
        
negtv:

        neg eax
        PRINT_STRING "-"
pos:
        
        PRINT_UDEC 4,eax
   
final:  
       
        mov esp,ebp
        pop ebp
	ret
